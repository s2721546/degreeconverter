package nl.utwente.di.bookQuote;

public class CelsiustoFahrenheit {
    private double degree = 0L;
    private double fahrenheit;
    public CelsiustoFahrenheit(){

    }
    public double returnFahrenheit(String degree){
        return Double.parseDouble(degree) * 1.8 + 32;
    }
}
